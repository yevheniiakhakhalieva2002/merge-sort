package simple_merge_sorting;

public class SimpleMergeSort {

    private void mergeArray(int[] array, int start, int mid, int end){
        if(array[mid]<array[mid+1]){
            if (end + 1 - start >= 0){
               System.arraycopy(array, start, array, start, end + 1 - start);
            }
        }else{
            int[] tempArray = new int[end-start+1];
            int i = start;
            int j = mid+1;
            int n = 0;
            while(i<=mid && j<=end){
                if(array[i]<=array[j]){
                    tempArray[n] = array[i];
                    i++;
                }else{
                    tempArray[n] = array[j];
                    j++;
                }
                n++;
            }
            while(i<=mid){
                tempArray[n] = array[i];
                n++;
                i++;
            }
            while(j<=end){
                tempArray[n] = array[j];
                n++;
                j++;
            }
            for(i = start; i <= end; i++){
                array[i] = tempArray[i-start];
            }
        }

    }

    public void sort(int[] array, int start, int end){
        if(start<end){
            int mid = ((start + end) / 2);
            sort(array,start,mid);
            sort(array,mid+1,end);
            mergeArray(array,start,mid,end);
        }
    }
}
