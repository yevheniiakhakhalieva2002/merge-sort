package simple_merge_sorting;

import java.util.Random;

public class Main {
    private static final int SIZE = 10;
    public static void main(String[] args) {
        int[] array = new int[SIZE];
        arrayFilling(array);
       // printArray(array);
        long startTime = System.currentTimeMillis();
        SimpleMergeSort mergeSort = new SimpleMergeSort();
        mergeSort.sort(array,0,array.length-1);
        long endTime = System.currentTimeMillis();
        System.out.println("After sorting: " + (endTime-startTime) + "ms");
       // printArray(array);
        long printingTime = System.currentTimeMillis();
        System.out.println("Printing time: " + (printingTime-endTime));

    }
    private static void arrayFilling(int[] array){
        Random random = new Random();
        for(int i = 0; i < array.length; i++){
            array[i] = random.nextInt(30);
        }
    }
    private static void printArray(int[] array){

        for (int value : array) {
            System.out.print(value+" ");
        }
        System.out.println();
        System.out.println();
    }
}
