package multithreaded_merge_sorting;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;

public class MergeSort extends RecursiveAction {
    private int[] array;
    private int start;
    private int end;
    private static final int THRESHOLD = 20;

    public MergeSort(int[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }
    @Override
    protected void compute() {
        if(end-start+1<=THRESHOLD){
            Arrays.sort(array,start,end+1);

        }else{

            if(start != end){
                int mid = ((start + end) / 2);
                MergeSort mergeSort1 = new MergeSort(array,start,mid);
                MergeSort mergeSort2 = new MergeSort(array,mid+1,end);
                invokeAll(mergeSort1,mergeSort2);
                mergeArray(start,mid,end);
            }
        }


    }
    private void mergeArray(int start, int mid, int end){
        if(array[mid]<=array[mid+1]){
            if (end-start+1 >= 0){
            }
        }else{
            int[] tempArray = new int[end-start+1];
            int i = start;
            int j = mid+1;
            int n = 0;
            while(i<=mid && j<=end){
                if(array[i]<=array[j]){
                    tempArray[n] = array[i];
                    i++;
                }else{
                    tempArray[n] = array[j];
                    j++;
                }
                n++;
            }
            while(i<=mid){
                tempArray[n] = array[i];
                n++;
                i++;
            }
            while(j<=end){
                tempArray[n] = array[j];
                n++;
                j++;
            }
            for(i = start; i <= end; i++){
                array[i] = tempArray[i-start];
            }
        }

    }
}
