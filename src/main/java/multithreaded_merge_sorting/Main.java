package multithreaded_merge_sorting;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class Main {
    private static final int SIZE = 262144;
    public static void main(String[] args) {
        int[] array = new int[SIZE];
        arrayFilling(array);
        printArray(array);
        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
        MergeSort mergeSort = new MergeSort(array,0,array.length-1);
        long startTime = System.currentTimeMillis();
        forkJoinPool.invoke(mergeSort);
        long endTime = System.currentTimeMillis();
        System.out.println("After sorting: " + (endTime-startTime) + "ms");
        printArray(array);
        long printingTime = System.currentTimeMillis();
        System.out.println("Printing time: " + (printingTime-endTime));

        forkJoinPool.shutdown();


    }
    private static void arrayFilling(int[] array){
        Random random = new Random();
        for(int i = 0; i < array.length; i++){
            array[i] = random.nextInt(1000);
        }
    }
    private static void printArray(int[] array){

        for (int value : array) {
            System.out.print(value+" ");
        }
        System.out.println();
        System.out.println();
    }
}
